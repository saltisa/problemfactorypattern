package lt.pfp.DevTrip.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lt.pfp.DevTrip.entities.AppUser;
import lt.pfp.DevTrip.services.UserService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static lt.pfp.DevTrip.auth.SecurityConstants.*;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
    private UserService userService;

    public JWTAuthorizationFilter(AuthenticationManager authManager, UserService userService) {
        super(authManager);
        this.userService = userService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(HEADER_STRING);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        String token = header.replace(TOKEN_PREFIX, "");

        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET.getBytes())
                    .parseClaimsJws(token)
                    .getBody();

            String username = claims.getSubject();
            if (username != null) {
//                @SuppressWarnings("unchecked")
//                List<String> authorities = (List<String>) claims.get("authorities");
//                List<GrantedAuthority> authorityList = authorities == null ? null : authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
                List<GrantedAuthority> authorityList = new ArrayList<>();
                AppUser appUser = userService.findByUsername(username);
                appUser.getUserRoleEnumList().forEach(userRoleEnum -> {
                    authorityList.add(new SimpleGrantedAuthority(userRoleEnum.name()));
                });

                UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                        username, null, authorityList);

                SecurityContextHolder.getContext().setAuthentication(auth);
            }

        } catch (Exception e) {
            SecurityContextHolder.clearContext();
        }

        chain.doFilter(request, response);
    }
}
