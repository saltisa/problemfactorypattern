package lt.pfp.DevTrip.auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "security.jwt")
public class SecurityConstants {
    public static String SECRET;
    public static long EXPIRATION_TIME;
    public static String TOKEN_PREFIX = "Bearer ";
    public static String HEADER_STRING = "Authorization";


    @Value("${secret:SecretKeyToGenJWTs}")
    public void setSecret(String secret) {
        SECRET = secret;
    }

    @Value("${expirationTime:864000000}")
    public void setExpirationTime(long expirationTime) {
        EXPIRATION_TIME = expirationTime;
    }
}
