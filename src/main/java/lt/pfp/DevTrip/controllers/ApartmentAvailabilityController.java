package lt.pfp.DevTrip.controllers;

import lt.pfp.DevTrip.entities.ApartmentAvailabilityCalendar;
import lt.pfp.DevTrip.services.ApartmentAvailabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class ApartmentAvailabilityController {

    private final ApartmentAvailabilityService apartmentAvailabilityService;

    @Autowired
    public ApartmentAvailabilityController(ApartmentAvailabilityService apartmentAvailabilityService) {
        this.apartmentAvailabilityService = apartmentAvailabilityService;
    }

    @RequestMapping(value = "aptAvailability", method = RequestMethod.PUT)
    public ApartmentAvailabilityCalendar save(@RequestBody ApartmentAvailabilityCalendar apartmentAvailabilityCalendar) {
        return apartmentAvailabilityService.saveApartmentAvailability(apartmentAvailabilityCalendar);
    }

    @RequestMapping(value = "aptAvailability/{aptId}", method = RequestMethod.GET)
    public List<ApartmentAvailabilityCalendar> getApartmentAvailabilityDates(@PathVariable("aptId") UUID aptId) {
        return apartmentAvailabilityService.getApartmentAvailability(aptId);
    }

    @RequestMapping(value = "aptAvailability", method = RequestMethod.POST)
    public List<ApartmentAvailabilityCalendar> getAllDates() {
        return apartmentAvailabilityService.getAllApartmentDates();
    }
}