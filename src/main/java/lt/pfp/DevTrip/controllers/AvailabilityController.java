package lt.pfp.DevTrip.controllers;

import lt.pfp.DevTrip.entities.AvailabilityCalendar;
import lt.pfp.DevTrip.services.AvailabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
public class AvailabilityController {
    private final AvailabilityService availabilityService;

    @Autowired
    public AvailabilityController(AvailabilityService availabilityService) {
        this.availabilityService = availabilityService;
    }

    @RequestMapping(value = "availability", method = RequestMethod.PUT)
    public List<AvailabilityCalendar> save(@RequestBody List<AvailabilityCalendar> list) {
        return availabilityService.save(list);
    }

    @RequestMapping(value = "availability", method = RequestMethod.POST)
    public List<AvailabilityCalendar> list() {
        return availabilityService.list();
    }

    @RequestMapping(value = "availability/{uuid}", method = RequestMethod.GET)
    public AvailabilityCalendar get(@PathVariable("uuid") UUID uuid) {
        return availabilityService.get(uuid);
    }

    @RequestMapping(value = "availability", method = RequestMethod.GET)
    public Boolean isUserAvailable(@RequestParam("uuid") UUID uuid, @RequestParam("dateFrom")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateFrom, @RequestParam("dateTo")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateTo) {
        return availabilityService.isUserAvailable(uuid, dateFrom, dateTo);
    }
}