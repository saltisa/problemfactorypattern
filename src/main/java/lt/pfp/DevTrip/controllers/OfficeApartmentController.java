package lt.pfp.DevTrip.controllers;

import lt.pfp.DevTrip.entities.OfficeApartment;
import lt.pfp.DevTrip.services.OfficeApartmentService;
import lt.pfp.DevTrip.services.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class OfficeApartmentController {

    private OfficeApartmentService officeApartmentService;
    private OfficeService officeService;

    @Autowired
    public OfficeApartmentController(
            OfficeApartmentService officeApartmentService,
            OfficeService officeService
    ) {
        this.officeApartmentService = officeApartmentService;
        this.officeService = officeService;
    }

    @RequestMapping(value = "officeApartment/{officeApartmentId}", method = RequestMethod.GET)
    public OfficeApartment findOfficeById(@PathVariable("officeApartmentId") UUID officeApartmentId) {
        return officeApartmentService.findOfficeApartmentById(officeApartmentId);
    }

    @RequestMapping(value = "officeApartment", method = RequestMethod.PUT)
    public List<OfficeApartment> saveOffice(@RequestBody List<OfficeApartment> officeApartments) {
        return officeApartmentService.saveOfficeApartments(officeApartments);
    }

    @RequestMapping(value = "officeApartment/{officeId}/add", method = RequestMethod.PUT)
    public OfficeApartment saveSingleOffice(
            @PathVariable("officeId") UUID officeId,
            @RequestBody OfficeApartment officeApartment) throws Exception {
        return officeService.findMappableOfficeByUuid(officeId).map(office -> {
            officeApartment.setOffice(office);
            return officeApartmentService.saveSingleOfficeApartment(officeApartment);
        }).orElseThrow(() -> new Exception("PostId " + officeId + " not found"));
    }

    @RequestMapping(value = "officeApartment", method = RequestMethod.POST)
    public List<OfficeApartment> getAllOffices() {
        return officeApartmentService.getAllOfficeApartments();
    }

    @RequestMapping(value = "officeApartment/{officeApartmentId}/changeAvailability", method = RequestMethod.POST)
    public OfficeApartment changeOfficeApartmentAvailability(
            @PathVariable("officeApartmentId") UUID officeApartmentId,
            @RequestParam("isAvailable") Boolean isAvailable
    ) {
        return officeApartmentService.setApartmentAvailability(officeApartmentId, isAvailable);
    }

}
