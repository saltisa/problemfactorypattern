package lt.pfp.DevTrip.controllers;

import lt.pfp.DevTrip.entities.Office;
import lt.pfp.DevTrip.services.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class OfficeController {

    private OfficeService officeService;

    @Autowired
    public OfficeController(OfficeService officeService) {
        this.officeService = officeService;
    }

    @RequestMapping(value = "office/{officeId}", method = RequestMethod.GET)
    public Office findOfficeById(@PathVariable("officeId") UUID officeId) {
        return officeService.findOfficeById(officeId);
    }

    @RequestMapping(value = "office", method = RequestMethod.PUT)
    public List<Office> saveOffice(@RequestBody List<Office> offices) {
        return officeService.saveOffice(offices);
    }

    @RequestMapping(value = "office/add", method = RequestMethod.PUT)
    public Office saveSingleOffice(@RequestBody Office office) {
        return officeService.saveSingleOffice(office);
    }

    @RequestMapping(value = "office", method = RequestMethod.POST)
    public List<Office> getAllOffices() {
        return officeService.getAllOffices();
    }
}

