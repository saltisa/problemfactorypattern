package lt.pfp.DevTrip.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RootController {

    // URL path
    @GetMapping("/")
    public String root() {
        return "index"; // html file name
    }

    @GetMapping("/index")
    public String index() {
        return "index";
    }
}
