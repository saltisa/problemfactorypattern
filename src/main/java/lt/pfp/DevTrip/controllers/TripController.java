package lt.pfp.DevTrip.controllers;

import lt.pfp.DevTrip.entities.Trip;
import lt.pfp.DevTrip.entities.TripEmployeeDetails;
import lt.pfp.DevTrip.services.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class TripController {

    private TripService tripService;

    @Autowired
    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @RequestMapping(value = "trip/{tripId}", method = RequestMethod.GET)
    public Trip findTripById(@PathVariable("tripId") UUID tripId) {
        return tripService.findTripById(tripId);
    }

    @RequestMapping(value = "usertrips/{userId}", method = RequestMethod.GET)
    public List<Trip> findUserTrips(@PathVariable("userId") UUID userId) {
        return tripService.findUserTrips(userId);
    }

    @RequestMapping(value = "trip/shouldConnect", method = RequestMethod.PUT)
    public UUID checkIfTripsConnectionNeeded(@RequestBody Trip trip) {
        return tripService.checkIfTripNeedsToBeConnected(trip);
    }

    @RequestMapping(value = "trip/{shouldConnect}", method = RequestMethod.PUT)
    public Trip saveTrip(
            @PathVariable("shouldConnect") Boolean shouldConnect,
            @RequestBody Trip trip
    ) {
        return tripService.connectTripIfNeededAndSave(trip, shouldConnect);
    }

    @RequestMapping(value = "trip/setApts", method = RequestMethod.PUT)
    public List<TripEmployeeDetails> setApartmentsAndHotel(
            @RequestBody List<TripEmployeeDetails> tripdetails
    ) {
        return tripService.setApartmentsAndHotelsForUsers(tripdetails);
    }

    @RequestMapping(value = "trip", method = RequestMethod.POST)
    public List<Trip> getAllTrips() {
        return tripService.getAllTrips();
    }

    @RequestMapping(value = "trip/{tripId}/updateStatus", method = RequestMethod.POST)
    public Trip changeTripStatus(
            @PathVariable("tripId") UUID tripId,
            @RequestParam("status") String status
    ) {
        return tripService.updateTripStatus(tripId, status);
    }
}
