package lt.pfp.DevTrip.controllers;

import lt.pfp.DevTrip.entities.TripEmployeeDetails;
import lt.pfp.DevTrip.services.TripEmployeeDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class TripEmployeeDetailsController {

    private TripEmployeeDetailsService detailsService;

    @Autowired
    public TripEmployeeDetailsController(TripEmployeeDetailsService detailsService) {
        this.detailsService = detailsService;
    }

    @RequestMapping(value = "tripDetails", method = RequestMethod.GET)
    public List<TripEmployeeDetails> getAll() {
        return detailsService.getAllDetails();
    }

    @RequestMapping(value = "tripDetails/{tripId}", method = RequestMethod.GET)
    public List<TripEmployeeDetails> getDetailsById(
            @PathVariable("tripId") UUID tripId) {
        return detailsService.getAllDetailsForTrip(tripId);
    }

    @RequestMapping(value = "tripDetails/{tripId}/{userId}", method = RequestMethod.GET)
    public TripEmployeeDetails getDetailsById(
            @PathVariable("tripId") UUID tripId,
            @PathVariable("userId") UUID userId) {
        return detailsService.getDetailsForUser(tripId, userId);
    }

    @RequestMapping(value = "tripDetails/removeUnapproved/{tripId}", method = RequestMethod.DELETE)
    public void deleteUserFromTrip(
            @PathVariable("tripId") UUID tripId) {
        detailsService.removeUnapproved(tripId);
    }

    @RequestMapping(value = "tripDetails/save", method = RequestMethod.PUT)
    public void putDetails(
            @RequestBody TripEmployeeDetails details) {
        detailsService.saveDetails(details);
    }

    @RequestMapping(value = "tripDetails/create", method = RequestMethod.POST)
    public void postDetails(
            @RequestBody List<TripEmployeeDetails> details) {
        detailsService.saveAllDetails(details);
    }
}
