package lt.pfp.DevTrip.controllers;

import lt.pfp.DevTrip.entities.AppUser;
import lt.pfp.DevTrip.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "user/{username}", method = RequestMethod.GET)
    public AppUser findByUsername(@PathVariable("username") String username) {
        return userService.findByUsername(username);
    }

    @RequestMapping(value = "user", method = RequestMethod.PUT)
    public List<AppUser> save(@RequestBody List<AppUser> appUserList) {
        return userService.saveUsers(appUserList);
    }

    @RequestMapping(value = "user/add", method = RequestMethod.PUT)
    public AppUser saveSingleUser(@RequestBody AppUser appUser) {
        return userService.saveSingleUser(appUser);
    }

    @RequestMapping(value = "user", method = RequestMethod.POST)
    public List<AppUser> list() {
        return userService.getAllUsers();
    }

    @RequestMapping(value = "user/changePassword", method = RequestMethod.POST)
    public AppUser changePassword(@RequestBody AppUser appUser, @RequestParam("oldPassword") String oldPassword, @RequestParam("newPassword") String newPassword) {
        return userService.changeUsersPassword(appUser, oldPassword, newPassword);
    }
}
