package lt.pfp.DevTrip.controllers.init;

import lt.pfp.DevTrip.dao.userDao.UserDao;
import lt.pfp.DevTrip.entities.AppUser;
import lt.pfp.DevTrip.entities.AvailabilityCalendar;
import lt.pfp.DevTrip.services.AvailabilityService;
import lt.pfp.DevTrip.services.UserService;
import lt.pfp.DevTrip.utils.UserRoleEnum;
import org.omg.PortableInterceptor.USER_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;

@RestController
public class InitController {
    private UserDao userDao;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public InitController(UserDao userDao, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Call this to initialize database - systemUrl/api/init (localhost:8080/api/init)
     */
    @RequestMapping(method = RequestMethod.GET, value = "/init")
    public void init() {
        if (userDao.findByUsername("admin") == null) {
            userDao.save(new AppUser(null, "adminName", "adminLastName",
                    "admin@email", null, "admin", bCryptPasswordEncoder.encode("admin"),
                    Arrays.asList(UserRoleEnum.ADMINISTRATOR, UserRoleEnum.ORGANIZATOR, UserRoleEnum.EMPLOYEE), null));
        }
    }
}
