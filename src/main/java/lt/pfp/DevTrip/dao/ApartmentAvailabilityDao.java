package lt.pfp.DevTrip.dao;

import lt.pfp.DevTrip.entities.ApartmentAvailabilityCalendar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ApartmentAvailabilityDao  extends JpaRepository<ApartmentAvailabilityCalendar, UUID> {
}
