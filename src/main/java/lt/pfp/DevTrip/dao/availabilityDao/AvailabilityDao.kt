package lt.pfp.DevTrip.dao.availabilityDao

import lt.pfp.DevTrip.entities.AvailabilityCalendar
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface AvailabilityDao: JpaRepository<AvailabilityCalendar, UUID>