package lt.pfp.DevTrip.dao.carDao

import lt.pfp.DevTrip.entities.Car
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CarDao: JpaRepository<Car, UUID>