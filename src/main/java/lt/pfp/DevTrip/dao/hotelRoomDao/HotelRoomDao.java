package lt.pfp.DevTrip.dao.hotelRoomDao;

import lt.pfp.DevTrip.entities.HotelRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface HotelRoomDao extends JpaRepository<HotelRoom, UUID> {

}
