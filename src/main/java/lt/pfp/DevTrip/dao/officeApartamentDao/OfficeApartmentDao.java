package lt.pfp.DevTrip.dao.officeApartamentDao;

import lt.pfp.DevTrip.entities.OfficeApartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface OfficeApartmentDao extends JpaRepository<OfficeApartment, UUID> {
    OfficeApartment findOffiiceApartmentByuuid(UUID uuid);
}
