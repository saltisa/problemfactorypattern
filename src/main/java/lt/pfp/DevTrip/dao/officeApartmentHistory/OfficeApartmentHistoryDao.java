package lt.pfp.DevTrip.dao.officeApartmentHistory;

import lt.pfp.DevTrip.entities.OfficeApartmentHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface OfficeApartmentHistoryDao extends JpaRepository<OfficeApartmentHistory, UUID> {
}
