package lt.pfp.DevTrip.dao.officeDao;

import lt.pfp.DevTrip.entities.Office;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OfficeDao  extends JpaRepository<Office, UUID> {
    Office findOffiiceByuuid(UUID uuid);
}