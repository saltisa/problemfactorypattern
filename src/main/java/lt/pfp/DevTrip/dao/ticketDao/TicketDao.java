package lt.pfp.DevTrip.dao.ticketDao;

import lt.pfp.DevTrip.entities.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TicketDao extends JpaRepository<Ticket, UUID> {
}
