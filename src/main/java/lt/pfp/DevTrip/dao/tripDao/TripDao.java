package lt.pfp.DevTrip.dao.tripDao;

import lt.pfp.DevTrip.entities.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TripDao extends JpaRepository<Trip, UUID> {
    Trip findTripByuuid(UUID uuid);
}
