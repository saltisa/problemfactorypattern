package lt.pfp.DevTrip.dao.tripEmployeeDetailsDao;

import lt.pfp.DevTrip.entities.Trip;
import lt.pfp.DevTrip.entities.TripEmployeeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TripEmployeeDetailsDao extends JpaRepository<TripEmployeeDetails, UUID> {
    @Query("SELECT d FROM TripEmployeeDetails d WHERE d.trip.uuid = :tripId")
    List<TripEmployeeDetails> findByTripId(@Param("tripId") UUID tripId);

    @Query("SELECT d FROM TripEmployeeDetails d WHERE d.trip.uuid = :tripId AND d.appUser.uuid = :userId")
    TripEmployeeDetails findByTripIdAndUserId(@Param("tripId") UUID tripId, @Param("userId") UUID userId);

    @Query("SELECT t FROM Trip t WHERE t.uuid IN (SELECT d.trip.uuid FROM TripEmployeeDetails d WHERE d.appUser.uuid = :userId)")
    List<Trip> findTripByUserId(@Param("userId") UUID userId);
}
