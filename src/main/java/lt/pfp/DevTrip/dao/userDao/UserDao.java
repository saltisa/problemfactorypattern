package lt.pfp.DevTrip.dao.userDao;

import lt.pfp.DevTrip.entities.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserDao extends JpaRepository<AppUser, UUID> {
    AppUser findByUsername(String username);
}
