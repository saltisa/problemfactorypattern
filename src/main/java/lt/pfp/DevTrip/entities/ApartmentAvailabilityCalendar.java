package lt.pfp.DevTrip.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
public class ApartmentAvailabilityCalendar extends UUIDEntity {
    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime unavailableFrom;

    public LocalDateTime getUnavailableFrom() {
        return unavailableFrom;
    }

    public void setUnavailableFrom(LocalDateTime unavailableFrom) {
        this.unavailableFrom = unavailableFrom;
    }

    public LocalDateTime getUnavailableTo() {
        return unavailableTo;
    }

    public void setUnavailableTo(LocalDateTime unavailableTo) {
        this.unavailableTo = unavailableTo;
    }

    public OfficeApartment getOfficeApartment() {
        return officeApartment;
    }

    public void setOfficeApartment(OfficeApartment officeApartment) {
        this.officeApartment = officeApartment;
    }

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime unavailableTo;

    @ManyToOne(optional = false)
    @JoinColumn(name = "fk_office_apartment")
    private OfficeApartment officeApartment;

    public ApartmentAvailabilityCalendar() {
    }

    @Builder
    public ApartmentAvailabilityCalendar(
            UUID uuid,
            LocalDateTime unavailableFrom,
            LocalDateTime unavailableTo,
            OfficeApartment officeApartment
    ) {
        super(uuid);
        this.unavailableFrom = unavailableFrom;
        this.unavailableTo = unavailableTo;
        this.officeApartment = officeApartment;
    }
}


