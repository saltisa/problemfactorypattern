package lt.pfp.DevTrip.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lt.pfp.DevTrip.utils.UserRoleEnum;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
public class AppUser extends UUIDEntity {
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String email;

    @ManyToOne
    @JoinColumn(name = "fk_office")
    private Office office;

    @Column(nullable = false)
    private String username;
    @Column(nullable = false)
    private String password;

    @Column
    @ElementCollection(targetClass = UserRoleEnum.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @JoinTable(name = "USER_ROLE",
            joinColumns = @JoinColumn(name = "USER_ID")
    )
    private List<UserRoleEnum> userRoleEnumList = new ArrayList<>();

    @OneToMany(mappedBy = "appUser", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<AvailabilityCalendar> availabilityCalendarSet;

    public AppUser() {
    }

    @Builder
    public AppUser(UUID uuid, String name, String lastName, String email, Office office, String username, String password, List<UserRoleEnum> userRoleEnumList, Set<AvailabilityCalendar> availabilityCalendarSet) {
        super(uuid);
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.office = office;
        this.username = username;
        this.password = password;
        this.userRoleEnumList = userRoleEnumList;
        this.availabilityCalendarSet = availabilityCalendarSet;
    }
}
