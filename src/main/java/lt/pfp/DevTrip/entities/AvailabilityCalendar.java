package lt.pfp.DevTrip.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
public class AvailabilityCalendar extends UUIDEntity {
    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime unavailableFrom;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime unavailableTo;

    @Column(nullable = false)
    private String reason;

    @ManyToOne(optional = false)
    @JoinColumn(name="fk_employee")
    private AppUser appUser;

    public AvailabilityCalendar() {
    }

    @Builder
    public AvailabilityCalendar(UUID uuid, LocalDateTime unavailableFrom, LocalDateTime unavailableTo, String reason, AppUser appUser) {
        super(uuid);
        this.unavailableFrom = unavailableFrom;
        this.unavailableTo = unavailableTo;
        this.reason = reason;
        this.appUser = appUser;
    }
}
