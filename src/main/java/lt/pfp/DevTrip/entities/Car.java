package lt.pfp.DevTrip.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Getter
@Setter
public class Car extends UUIDEntity {
    @Column(nullable = false)
    private Timestamp reservedFrom;

    @Column(nullable = false)
    private Timestamp reservedTo;

    @Column(nullable = false)
    private String licensePlate;

    @Column(nullable = false)
    private String model;

    @Column(nullable = false)
    private String year;

    @Column(nullable = false)
    private float price;

    public Car() {
    }

    @Builder
    public Car(UUID uuid, Timestamp reservedFrom, Timestamp reservedTo, String licensePlate, String model, String year, float price) {
        super(uuid);
        this.reservedFrom = reservedFrom;
        this.reservedTo = reservedTo;
        this.licensePlate = licensePlate;
        this.model = model;
        this.year = year;
        this.price = price;
    }
}
