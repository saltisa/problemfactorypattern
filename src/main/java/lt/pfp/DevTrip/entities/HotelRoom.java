package lt.pfp.DevTrip.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Getter
@Setter
public class HotelRoom extends UUIDEntity {
    @Column(nullable = false)
    private Timestamp occupiedFrom;

    @Column(nullable = false)
    private Timestamp occupiedTo;

    @Column(nullable = false)
    private String streetAddress;

    @Column(nullable = false)
    private float price;

    public HotelRoom() {
    }

    @Builder
    public HotelRoom(UUID uuid, Timestamp occupiedFrom, Timestamp occupiedTo, String streetAddress, float price) {
        super(uuid);
        this.occupiedFrom = occupiedFrom;
        this.occupiedTo = occupiedTo;
        this.streetAddress = streetAddress;
        this.price = price;
    }
}
