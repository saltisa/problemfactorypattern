package lt.pfp.DevTrip.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.UUID;

@Entity
@Getter
@Setter
public class Office extends UUIDEntity {
    @Column(nullable = false)
    private String country;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String streetAddress;

    public Office() {
    }

    @Builder
    public Office(UUID uuid, String country, String city, String streetAddress) {
        super(uuid);
        this.country = country;
        this.city = city;
        this.streetAddress = streetAddress;
    }
}
