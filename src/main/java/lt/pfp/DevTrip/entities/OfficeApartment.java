package lt.pfp.DevTrip.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Entity
@Getter
@Setter
public class OfficeApartment extends UUIDEntity {
    @ManyToOne
    @JoinColumn(name = "fk_office")
    private Office office;

    @Column(nullable = false)
    private String streetAddress;

    @Column
    private Boolean isApartmentTaken;

    @Column(nullable = false)
    private int apartmentNumber;

    public OfficeApartment() {
    }

    @Builder
    public OfficeApartment(UUID uuid, Office office, String streetAddress) {
        super(uuid);
        this.office = office;
        this.streetAddress = streetAddress;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public void setApartmentTaken(Boolean apartmentTaken) {
        isApartmentTaken = apartmentTaken;
    }

    public Office getOffice() {
        return office;
    }
}
