package lt.pfp.DevTrip.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
public class OfficeApartmentHistory extends UUIDEntity {

    //TODO
    //Sitas Entity man labai primena ManyToMany sarysi
    // tarp OfficeApartmentBed ir Trip

    @ManyToOne(optional = false)
    @JoinColumn(name = "fk_officeApartmentBed")
    private OfficeApartment officeApartment;

    @OneToOne(optional = false)
    @JoinColumn(name = "fk_trip")
    private Trip trip;

    public OfficeApartmentHistory() {
    }

    @Builder
    public OfficeApartmentHistory(UUID uuid, OfficeApartment officeApartment, Trip trip) {
        super(uuid);
        this.officeApartment = officeApartment;
        this.trip = trip;
    }
}
