package lt.pfp.DevTrip.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Getter
@Setter
public class Ticket extends UUIDEntity {
    @Column(nullable = false)
    private String type;

    @Column(nullable = false)
    private Timestamp departure;

    @Column(nullable = false)
    private Timestamp arrival;

    @Column(nullable = false)
    private String seat;

    @Column(nullable = false)
    private float price;

    @Column(nullable = false)
    private String additionalInfo;

    public Ticket() {
    }

    @Builder
    public Ticket(UUID uuid, String type, Timestamp departure, Timestamp arrival, String seat, float price, String additionalInfo) {
        super(uuid);
        this.type = type;
        this.departure = departure;
        this.arrival = arrival;
        this.seat = seat;
        this.price = price;
        this.additionalInfo = additionalInfo;
    }
}
