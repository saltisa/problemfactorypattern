package lt.pfp.DevTrip.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
public class Trip extends UUIDEntity {

    @ManyToOne
    @JoinColumn(name = "fk_departure_office")
    private Office departureOffice;

    @Column
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime departureDate;

    @Column
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime returnDate;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "fk_employee")
    private AppUser coordinator;

    @Column(nullable = false)
    private String status;

    public Office getDepartureOffice() {
        return departureOffice;
    }

    public void setDepartureOffice(Office departureOffice) {
        this.departureOffice = departureOffice;
    }

    public LocalDateTime getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDateTime departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDateTime returnDate) {
        this.returnDate = returnDate;
    }

    public AppUser getCoordinator() {
        return coordinator;
    }

    public void setCoordinator(AppUser coordinator) {
        this.coordinator = coordinator;
    }

    public String getStatus() {
        return status;
    }

    public Office getDestinationOffice() {
        return destinationOffice;
    }

    public void setDestinationOffice(Office destinationOffice) {
        this.destinationOffice = destinationOffice;
    }

    @ManyToOne
    @JoinColumn(name = "fk_destination_office")
    private Office destinationOffice;

    public Trip() {
    }

    @Builder
    public Trip(UUID uuid,
                Office departureOffice,
                LocalDateTime departureDate,
                LocalDateTime returnDate,
                AppUser coordinator,
                String status,
                Office destinationOffice) {
        super(uuid);
        this.departureOffice = departureOffice;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.coordinator = coordinator;
        this.status = status;
        this.destinationOffice = destinationOffice;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
