package lt.pfp.DevTrip.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"fk_employee", "fk_trip"}))
public class TripEmployeeDetails extends UUIDEntity {
    @ManyToOne(optional = false)
    @JoinColumn(name = "fk_employee")
    private AppUser appUser;

    @ManyToOne(optional = false)
    @JoinColumn(name = "fk_trip")
    private Trip trip;

    @Column
    private boolean isCarNeeded;

    @Column
    private boolean isHotelNeeded;

    @Column
    private boolean isTicketNeeded;

    @Column
    private boolean isApproved;

    @OneToOne
    @JoinColumn(name = "fk_officeApartment")
    private OfficeApartment officeApartment;

    public TripEmployeeDetails() {
    }

    @Builder
    public TripEmployeeDetails(UUID uuid,
                               AppUser appUser,
                               Trip trip,
                               boolean isCarNeeded,
                               boolean isTicketNeeded,
                               boolean isApproved,
                               OfficeApartment officeApartment,
                               boolean isHotelNeeded) {
        super(uuid);
        this.appUser = appUser;
        this.trip = trip;
        this.isCarNeeded = isCarNeeded;
        this.officeApartment = officeApartment;
        this.isHotelNeeded = isHotelNeeded;
        this.isTicketNeeded = isTicketNeeded;
        this.isApproved = isApproved;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public boolean isCarNeeded() {
        return isCarNeeded;
    }

    public void setCarNeeded(boolean carNeeded) {
        isCarNeeded = carNeeded;
    }

    public boolean isHotelNeeded() {
        return isHotelNeeded;
    }

    public void setHotelNeeded(boolean hotelNeeded) {
        isHotelNeeded = hotelNeeded;
    }

    public boolean isTicketNeeded() {
        return isTicketNeeded;
    }

    public void setTicketNeeded(boolean ticketNeeded) {
        isTicketNeeded = ticketNeeded;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }

    public OfficeApartment getOfficeApartment() {
        return officeApartment;
    }

    public void setOfficeApartment(OfficeApartment officeApartment) {
        this.officeApartment = officeApartment;
    }

}
