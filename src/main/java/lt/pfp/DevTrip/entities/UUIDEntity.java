package lt.pfp.DevTrip.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public abstract class UUIDEntity implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "uuid", updatable = false, nullable = false, unique = true)
    @Type(type = "uuid-char")
    private UUID uuid;

    public UUID getUuid() {
        return uuid;
    }
}
