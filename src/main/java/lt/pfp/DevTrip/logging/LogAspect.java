package lt.pfp.DevTrip.logging;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.apache.log4j.Logger;

@Aspect
@Component
public class LogAspect {

    @Before("execution(* lt.pfp.DevTrip.controllers.*.*(..))")
    public void log(JoinPoint joinPoint) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        final Class<?> targetClass = joinPoint.getTarget().getClass();

        final Logger logger = Logger.getLogger(targetClass);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("user:");
        stringBuilder.append(auth.getName());
        stringBuilder.append(";roles:");
        for (GrantedAuthority authority : auth.getAuthorities()) {
            stringBuilder.append(authority.getAuthority());
            stringBuilder.append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.append(";method:");
        stringBuilder.append(joinPoint.getSignature().getName());

        logger.info(stringBuilder.toString());
    }
}