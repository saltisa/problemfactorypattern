package lt.pfp.DevTrip.services

import lt.pfp.DevTrip.entities.ApartmentAvailabilityCalendar
import java.util.*

interface ApartmentAvailabilityService {

    fun getApartmentAvailability(apartmentUuid: UUID): List<ApartmentAvailabilityCalendar>

    fun saveApartmentAvailability(apartmentAvailabilityCalendar: ApartmentAvailabilityCalendar): ApartmentAvailabilityCalendar

    fun getAllApartmentDates(): List<ApartmentAvailabilityCalendar>

}