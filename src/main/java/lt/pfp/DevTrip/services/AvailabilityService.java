package lt.pfp.DevTrip.services;

import lt.pfp.DevTrip.entities.AppUser;
import lt.pfp.DevTrip.entities.AvailabilityCalendar;
import org.springframework.security.core.userdetails.User;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface AvailabilityService {
    List<AvailabilityCalendar> save(List<AvailabilityCalendar> list);

    List<AvailabilityCalendar> list();

    AvailabilityCalendar get(UUID uuid);

    Boolean isUserAvailable(UUID uuid, LocalDateTime dateFrom, LocalDateTime dateTo);
}
