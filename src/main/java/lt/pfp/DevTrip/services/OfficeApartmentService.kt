package lt.pfp.DevTrip.services

import lt.pfp.DevTrip.entities.OfficeApartment
import java.util.*

interface OfficeApartmentService {

    fun getAllOfficeApartments(): List<OfficeApartment>

    fun saveOfficeApartments(apartmentList: List<OfficeApartment>): List<OfficeApartment>

    fun saveSingleOfficeApartment(apartment: OfficeApartment): OfficeApartment

    fun findOfficeApartmentById(apartmentId: UUID): OfficeApartment

    fun deleteOfficeApartment(apartmentId: UUID)

    fun setApartmentAvailability(apartmentId: UUID, isAvailable: Boolean): OfficeApartment
}