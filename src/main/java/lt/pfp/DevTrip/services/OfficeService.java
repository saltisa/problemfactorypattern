package lt.pfp.DevTrip.services;

import lt.pfp.DevTrip.entities.Office;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeService {

    List<Office> getAllOffices();

    List<Office> saveOffice(List<Office> officeList);

    Office saveSingleOffice(Office office);

    Office findOfficeById(UUID officeId);

    Optional<Office> findMappableOfficeByUuid(UUID officeId);

    void deleteOffice(UUID officeId);

}
