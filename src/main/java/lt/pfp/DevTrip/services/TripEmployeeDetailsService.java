package lt.pfp.DevTrip.services;

import lt.pfp.DevTrip.entities.TripEmployeeDetails;

import java.util.List;
import java.util.UUID;

public interface TripEmployeeDetailsService {

    void saveAllDetails(List<TripEmployeeDetails> list);

    void saveDetails(TripEmployeeDetails details);

    List<TripEmployeeDetails> getAllDetailsForTrip(UUID tripId);

    List<TripEmployeeDetails> getAllDetails();

    TripEmployeeDetails getDetailsForUser(UUID tripId, UUID userId);

    void removeUnapproved(UUID tripId);
}

