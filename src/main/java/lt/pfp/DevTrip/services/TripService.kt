package lt.pfp.DevTrip.services

import lt.pfp.DevTrip.entities.AppUser
import lt.pfp.DevTrip.entities.Office
import lt.pfp.DevTrip.entities.Trip
import lt.pfp.DevTrip.entities.TripEmployeeDetails
import java.time.LocalDateTime
import java.util.UUID

interface TripService {

    fun  getAllTrips(): List<Trip>

    fun saveTrip(tripList: Trip): Trip

    fun findTripById(tripId: UUID): Trip

    fun findUserTrips(userId: UUID): List<Trip>?

    fun updateTripStatus(tripId: UUID, status: String): Trip

    fun connectTripIfNeededAndSave(trip: Trip, connectTrip: Boolean): Trip?

    fun setApartmentsAndHotelsForUsers(tripsDetails: List<TripEmployeeDetails>): List<TripEmployeeDetails>

    fun checkIfTripNeedsToBeConnected(trip: Trip): UUID?

    fun deleteTrip(TripId: UUID)
}
