package lt.pfp.DevTrip.services;

import lt.pfp.DevTrip.entities.AppUser;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.UUID;

public interface UserService extends UserDetailsService {
    List<AppUser> saveUsers(List<AppUser> appUserList);

    List<AppUser> getAllUsers();

    AppUser findByUsername(String username);

    AppUser changeUsersPassword(AppUser appUser, String oldPassword, String newPassword);

    AppUser saveSingleUser(AppUser appUser);

    AppUser get(UUID uuid);
}
