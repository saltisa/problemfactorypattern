package lt.pfp.DevTrip.services.impl

import lt.pfp.DevTrip.dao.ApartmentAvailabilityDao
import lt.pfp.DevTrip.entities.ApartmentAvailabilityCalendar
import lt.pfp.DevTrip.services.ApartmentAvailabilityService
import org.springframework.stereotype.Service
import java.util.*

@Service
class ApartmentAvailabilityServiceImpl(val apartmentAvailabilityDao: ApartmentAvailabilityDao): ApartmentAvailabilityService {

    override fun getApartmentAvailability(apartmentUuid: UUID): List<ApartmentAvailabilityCalendar> {
        val allDates = getAllApartmentDates()
        return allDates.filter { it.uuid == apartmentUuid }
    }

    override fun saveApartmentAvailability(apartmentAvailabilityCalendar: ApartmentAvailabilityCalendar): ApartmentAvailabilityCalendar {
        return apartmentAvailabilityDao.save(apartmentAvailabilityCalendar)
    }

    override fun getAllApartmentDates(): List<ApartmentAvailabilityCalendar> {
        return apartmentAvailabilityDao.findAll()
    }
}