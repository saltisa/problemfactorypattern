package lt.pfp.DevTrip.services.impl;

import lt.pfp.DevTrip.dao.availabilityDao.AvailabilityDao;
import lt.pfp.DevTrip.dao.userDao.UserDao;
import lt.pfp.DevTrip.entities.AppUser;
import lt.pfp.DevTrip.entities.AvailabilityCalendar;
import lt.pfp.DevTrip.services.AvailabilityService;
import org.postgresql.jdbc.TimestampUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AvailabilityServiceImpl implements AvailabilityService {

    private final AvailabilityDao availabilityDao;
    private final UserDao userDao;

    @Autowired
    public AvailabilityServiceImpl(AvailabilityDao availabilityDao, UserDao userDao) {
        this.availabilityDao = availabilityDao;
        this.userDao = userDao;
    }

    @Override
    public List<AvailabilityCalendar> save(List<AvailabilityCalendar> list) {
        return availabilityDao.saveAll(list);
    }

    @Override
    public List<AvailabilityCalendar> list() {
        return availabilityDao.findAll();
    }

    @Override
    public AvailabilityCalendar get(UUID uuid) {
        Optional<AvailabilityCalendar> availabilityCalendar = availabilityDao.findById(uuid);
        if (!availabilityCalendar.isPresent()) {
            throw new RuntimeException(String.format("AvailabilityCalendar with UUID '%s' does not exist", uuid));
        }
        return availabilityCalendar.get();
    }

    @Override
    public Boolean isUserAvailable(UUID uuid, LocalDateTime dateFrom, LocalDateTime dateTo) {
        AppUser user = userDao.findById(uuid).get();
        for (AvailabilityCalendar availabilityCalendar : user.getAvailabilityCalendarSet()) {
            if ((dateFrom.isAfter(availabilityCalendar.getUnavailableFrom()) && dateFrom.isBefore(availabilityCalendar.getUnavailableTo()))
                    || (dateTo.isAfter(availabilityCalendar.getUnavailableFrom()) && dateTo.isBefore(availabilityCalendar.getUnavailableTo()))
                    || (availabilityCalendar.getUnavailableFrom().isAfter(dateFrom) && availabilityCalendar.getUnavailableFrom().isBefore(dateTo))
                    || (availabilityCalendar.getUnavailableTo().isAfter(dateFrom) && availabilityCalendar.getUnavailableTo().isBefore(dateTo))) {
                return false;
            }
        }
        return true;
    }
}
