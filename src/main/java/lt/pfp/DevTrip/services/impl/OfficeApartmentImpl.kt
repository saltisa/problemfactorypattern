package lt.pfp.DevTrip.services.impl

import lt.pfp.DevTrip.dao.officeApartamentDao.OfficeApartmentDao
import lt.pfp.DevTrip.entities.OfficeApartment
import lt.pfp.DevTrip.services.OfficeApartmentService
import org.springframework.stereotype.Service
import java.util.*

@Service
class OfficeApartmentImpl(val officeApartmentDao: OfficeApartmentDao): OfficeApartmentService {

    override fun getAllOfficeApartments(): List<OfficeApartment> {
        return officeApartmentDao.findAll()
    }

    override fun saveOfficeApartments(officeApartmentList: List<OfficeApartment>): List<OfficeApartment> {
        return officeApartmentDao.saveAll(officeApartmentList)
    }

    override fun saveSingleOfficeApartment(officeApartment: OfficeApartment): OfficeApartment {
        return officeApartmentDao.save(officeApartment)
    }

    override fun findOfficeApartmentById(officeApartmentId: UUID): OfficeApartment {
        return officeApartmentDao.findOffiiceApartmentByuuid(officeApartmentId)
    }

    override fun deleteOfficeApartment(officeId: UUID) {
        officeApartmentDao.delete(officeApartmentDao.getOne(officeId))
    }

    override fun setApartmentAvailability(apartmentId: UUID, isAvailable: Boolean): OfficeApartment {
        val apartment = officeApartmentDao.findById(apartmentId).get()
        apartment.setApartmentTaken(isAvailable)
        return officeApartmentDao.save(apartment)
    }
}