package lt.pfp.DevTrip.services.impl;

import lt.pfp.DevTrip.dao.officeDao.OfficeDao;
import lt.pfp.DevTrip.entities.Office;
import lt.pfp.DevTrip.services.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OfficeServiceImpl implements OfficeService {

    private OfficeDao officeDao;

    @Autowired
    public OfficeServiceImpl(OfficeDao officeDao) {
        this.officeDao = officeDao;
    }

    @Override
    public List<Office> getAllOffices() {
        return officeDao.findAll();
    }

    @Override
    public List<Office> saveOffice(List<Office> officeList) {
        return officeDao.saveAll(officeList);
    }

    @Override
    public Office findOfficeById(UUID uuid) {
        return officeDao.findOffiiceByuuid(uuid);
    }


    @Override
    public Office saveSingleOffice(Office office) {
        return officeDao.save(office);
    }

    @Override
    public Optional<Office> findMappableOfficeByUuid(UUID officeId) {
        return officeDao.findById(officeId);
    }

    @Override
    public void deleteOffice(UUID officeId) {
        officeDao.delete(officeDao.getOne(officeId));
    }
}
