package lt.pfp.DevTrip.services.impl;

import lt.pfp.DevTrip.dao.tripEmployeeDetailsDao.TripEmployeeDetailsDao;
import lt.pfp.DevTrip.entities.TripEmployeeDetails;
import lt.pfp.DevTrip.services.TripEmployeeDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class TripEmployeeDetailsImpl implements TripEmployeeDetailsService{
    private TripEmployeeDetailsDao detailsDao;

    @Autowired
    public TripEmployeeDetailsImpl(TripEmployeeDetailsDao detailsDao) {
        this.detailsDao = detailsDao;
    }

    @Override
    public List<TripEmployeeDetails> getAllDetails() {
        return detailsDao.findAll();
    }

    @Override
    public void saveAllDetails(List<TripEmployeeDetails> list) {
        for (TripEmployeeDetails details :
                list) {
            detailsDao.saveAndFlush(details);
        }
    }

    @Override
    public void saveDetails(TripEmployeeDetails details) {
        detailsDao.save(details);
    }

    @Override
    public List<TripEmployeeDetails> getAllDetailsForTrip(UUID tripId) {
        return detailsDao.findByTripId(tripId);
    }

    @Override
    public TripEmployeeDetails getDetailsForUser(UUID tripId, UUID userId) {
        return detailsDao.findByTripIdAndUserId(tripId, userId);
    }

    @Override
    public void removeUnapproved(UUID tripId) {
        List<TripEmployeeDetails> detailsList = getAllDetailsForTrip(tripId);
        for (TripEmployeeDetails details: detailsList) {
            if(!details.isApproved()){
                detailsDao.delete(details);
            }
        }
    }

}
