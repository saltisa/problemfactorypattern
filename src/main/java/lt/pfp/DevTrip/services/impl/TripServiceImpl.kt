package lt.pfp.DevTrip.services.impl

import lt.pfp.DevTrip.dao.ApartmentAvailabilityDao
import lt.pfp.DevTrip.dao.officeApartamentDao.OfficeApartmentDao
import lt.pfp.DevTrip.dao.tripDao.TripDao
import lt.pfp.DevTrip.dao.tripEmployeeDetailsDao.TripEmployeeDetailsDao
import lt.pfp.DevTrip.entities.*
import lt.pfp.DevTrip.services.TripService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*

@Service
class TripServiceImpl @Autowired
constructor(
        private val tripDao: TripDao,
        private val tripEmployeeDetailsDao: TripEmployeeDetailsDao,
        private val officeApartmentAvailabilityDao: ApartmentAvailabilityDao,
        private val officeApartmentDao: OfficeApartmentDao
) : TripService {

    override fun getAllTrips(): List<Trip> {
        return tripDao.findAll()
    }

    override fun saveTrip(trip: Trip): Trip {
        return tripDao.save(trip)
    }

    override fun findTripById(tripId: UUID): Trip {
        return tripDao.findTripByuuid(tripId)
    }

    override fun findUserTrips(userId: UUID): List<Trip>? {
        return tripEmployeeDetailsDao.findTripByUserId(userId)
    }

    override fun deleteTrip(TripId: UUID) {

    }

    override fun connectTripIfNeededAndSave(trip: Trip, connectTrip: Boolean): Trip? {
        val allCurrentTrips = getAllTrips()
        val connectedTrip = getConnectingTripOrNull(allCurrentTrips, trip)
        return if (connectedTrip != null && connectTrip) {
            tripDao.save(connectedTrip)
        } else {
            tripDao.save(trip)
        }
    }

    override fun setApartmentsAndHotelsForUsers(tripsDetails: List<TripEmployeeDetails>): List<TripEmployeeDetails> {
        val destinationApartments = getAllOfficeApartment(tripsDetails.firstOrNull()?.trip?.destinationOffice?.uuid)
        val tripStartDate = tripsDetails.firstOrNull()?.trip?.departureDate
        val tripEndDate = tripsDetails.firstOrNull()?.trip?.returnDate
        tripsDetails.forEach { tripDetails ->
            loop@ for (apt in destinationApartments) {
                val aptAvailability = officeApartmentAvailabilityDao.findAll().filter { it.officeApartment.uuid == apt.uuid }
                if (aptAvailability.isEmpty()) {
                    tripDetails.officeApartment = apt
                    println(apt.toString() + "apt availability was empty")
                    saveAparmentAvailability(tripStartDate, tripEndDate, apt)
                    break@loop
                } else {
                    for (aptAvailabilityDate in aptAvailability) {
                        if (validateAptAv(aptAvailability,
                                        tripStartDate,
                                        tripEndDate)) {
                            saveAparmentAvailability(tripStartDate, tripEndDate, apt)
                            println(apt.toString() + "apt was available")
                            tripDetails.officeApartment = apt
                            break@loop
                        }
                    }
                }
            }
        }
        tripsDetails.forEach {
            if (it.officeApartment == null) {
                it.isHotelNeeded = true
                println(it.toString() + "Stays in hotel")
            }
        }
        return tripsDetails
    }

    private fun validateAptAv(
            aptAv: List<ApartmentAvailabilityCalendar>,
            tripStart: LocalDateTime?,
            tripEnd: LocalDateTime?
    ): Boolean {
        aptAv.forEach {
            if (!availabilityValidator(
                            it.unavailableFrom,
                            it.unavailableFrom,
                            tripStart,
                            tripEnd
                    )) {
                return false
            }
        }
        return true
    }

    private fun saveAparmentAvailability(tripStart: LocalDateTime?, tripEnd: LocalDateTime?, officeAparment: OfficeApartment) {
        officeApartmentAvailabilityDao.save(ApartmentAvailabilityCalendar(
                null,
                tripStart,
                tripEnd,
                officeAparment
        ))
    }

    private fun getAllOfficeApartment(officeUuid: UUID?): List<OfficeApartment> {
        return officeApartmentDao.findAll().filter { it.office.uuid == officeUuid }
    }

    override fun checkIfTripNeedsToBeConnected(trip: Trip): UUID? {
        val allCurrentTrips = getAllTrips()
        return getConnectingTripOrNull(allCurrentTrips, trip)?.uuid
    }

    override fun updateTripStatus(tripId: UUID, status: String): Trip {
        val trip = tripDao.findById(tripId).get()
        trip.status = status
        return tripDao.save(trip)
    }

    private fun availabilityValidator(
            aptStartDate: LocalDateTime,
            aptEndDate: LocalDateTime,
            tripStartDate: LocalDateTime?,
            tripEndDate: LocalDateTime?
    ): Boolean {
        if (aptStartDate < tripEndDate
                && aptStartDate < tripStartDate
                && aptEndDate < tripEndDate
                && aptEndDate < tripStartDate) {
            return true
        }

        if (aptStartDate > tripStartDate
                && aptStartDate > tripEndDate
                && aptEndDate > tripEndDate
                && aptEndDate > tripStartDate) {
            return true
        }
        return false
    }

    private fun getConnectingTripOrNull(allCurrentTrips: List<Trip>, trip: Trip): Trip? {
        return allCurrentTrips.find {
            it.destinationOffice.uuid == trip.destinationOffice.uuid
                    && it.departureOffice.uuid == trip.departureOffice.uuid
                    && ChronoUnit.DAYS.between(it.departureDate, trip.departureDate) <= 1
                    && ChronoUnit.DAYS.between(it.returnDate, trip.returnDate) <= 1
        }
    }
}
