package lt.pfp.DevTrip.services.impl;

import lt.pfp.DevTrip.dao.userDao.UserDao;
import lt.pfp.DevTrip.entities.AppUser;
import lt.pfp.DevTrip.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.Collections.emptyList;

@Service
public class UserServiceImpl implements UserService {
    private final UserDao userDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userDao, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser = userDao.findByUsername(username);
        if (appUser == null) {
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(appUser.getUsername(), appUser.getPassword(), emptyList());
    }

    @Override
    public List<AppUser> saveUsers(List<AppUser> appUserList) {
        appUserList.forEach(this::validate);
        return userDao.saveAll(appUserList);
    }

    @Override
    public List<AppUser> getAllUsers() {
        return userDao.findAll();
    }

    @Override
    public AppUser findByUsername(String username) {
        AppUser appUser = userDao.findByUsername(username);
        if (appUser == null) {
            throw new UsernameNotFoundException(username);
        }
        return appUser;
    }

    @Override
    public AppUser changeUsersPassword(AppUser appUser, String oldPassword, String newPassword) {
        if (bCryptPasswordEncoder.matches(oldPassword, appUser.getPassword())) {
            appUser.setPassword(bCryptPasswordEncoder.encode(newPassword));
        } else {
            throw new RuntimeException("Incorrect password");
        }
        return userDao.save(appUser);
    }

    private AppUser validate(AppUser appUser) {
        if (appUser.getUuid() == null) {
            if (userDao.findByUsername(appUser.getUsername()) != null) {
                throw new RuntimeException(String.format("AppUser with username '%s' already exists", appUser.getUsername()));
            }
            appUser.setPassword(bCryptPasswordEncoder.encode(appUser.getPassword()));
        }
        return appUser;
    }

    @Override
    public AppUser saveSingleUser(AppUser appUser) {
        return userDao.save(validate(appUser));
    }

    @Override
    public AppUser get(UUID uuid) {
        Optional<AppUser> user = userDao.findById(uuid);
        if (!user.isPresent()) {
            throw new RuntimeException(String.format("AppUser with UUID '%s' does not exist", uuid));
        }
        return user.get();
    }
}
