package lt.pfp.DevTrip.utils;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Builder
public class EmployeeFilter {
    private String name;
    private String lastName;
    private String email;
    private UUID office;

    public EmployeeFilter() {

    }

    public EmployeeFilter(String name, String lastName, String email, UUID office) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.office = office;
    }
}
